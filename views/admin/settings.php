<?php
    echo CHtml::form($form['action']);
?>
<h3 class="clearfix"><?php echo $lang['Webserver Authentication settings']; ?>
  <div class='pull-right'>
    <?php
      if(Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
        echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'save','class'=>'btn btn-primary'));
        echo " ";
        echo CHtml::htmlButton('<i class="fa fa-check-circle-o " aria-hidden="true"></i> '.gT('Save and close'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'redirect','class'=>'btn btn-default'));
        echo " ";
        echo CHtml::link(gT('Reset'),$form['reset'],array('class'=>'btn btn-danger'));
        echo " ";
        echo CHtml::link(gT('Close'),$form['close'],array('class'=>'btn btn-danger'));
      } else {
        echo CHtml::link(gT('Close'),$form['close'],array('class'=>'btn btn-default'));
      }
    ?>
  </div>
</h3>
<?php
if(!empty($errors)) {
  foreach($errors as $error) { ?>
    <p class="alert alert-danger"><?=$error?></p>
  <?php }
}
?>
<div>
    <?php foreach($aSettings as $legend=>$settings) {
    $this->widget('ext.SettingsWidget.SettingsWidget', array(
        'title'=>$legend,
        //'prefix' => $pluginClass, This break the label (id!=name)
        'form' => false,
        'formHtmlOptions'=>array(
            'class'=>'form-core',
        ),
        'labelWidth'=>6,
        'controlWidth'=>6,
        'settings' => $settings,
    ));
    } ?>
    <?php if(!empty($aSurveyPluginsSettings)) {
      echo CHtml::tag('fieldset',
        array(),
        CHtml::tag('legend', array(),gT('Settings by plugins')),
        false
      );
      foreach($aSurveyPluginsSettings as $id => $plugin)  {
        $title = sprintf(gT("Settings for plugin %s"), $plugin['name']);
        $fieldId = CHtml::getIdByName($title);
        $title = CHtml::link(
          "§" ,
          "#{$fieldId}",
          array(
            'class' => 'self-link',
            'name' => "{$fieldId}",
            'aria-label' => gT("Link")
          )
        ) . " " . $title;
        $this->widget('ext.SettingsWidget.SettingsWidget', array(
            'settings' => $plugin['settings'],
            'form' => false,
            'title' => $title,
            'prefix' => "plugin[{$plugin['name']}]",
        ));
      }
    } ?>
</div>
<?php 
/* The buttons #2 */
?>
<div class='row'>
  <div class='text-center submit-buttons'>
    <?php
      if(Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
        echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'save','class'=>'btn btn-primary'));
        echo " ";
        echo CHtml::htmlButton('<i class="fa fa-check-circle-o " aria-hidden="true"></i> '.gT('Save and close'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'redirect','class'=>'btn btn-default'));
        echo " ";
        echo CHtml::link(gT('Reset'),$form['reset'],array('class'=>'btn btn-danger'));
        echo " ";
        echo CHtml::link(gT('Close'),$form['close'],array('class'=>'btn btn-danger'));
      } else {
        echo CHtml::link(gT('Close'),$form['close'],array('class'=>'btn btn-default'));
      }
    ?>
  </div>
</div>

</form>
<?php
  Yii::app()->clientScript->registerPackage('bootstrap-switch', LSYii_ClientScript::POS_BEGIN);
  
?>
<script>
  window.LS.renderBootstrapSwitch();
</script>
