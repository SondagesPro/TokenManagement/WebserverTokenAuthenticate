# WebserverTokenAuthenticate

WebServer authentication for participant in LimeSurvey

## Documentation

Set WebServer authentication default authneticationfor participant in LimeSurvey on plugin settings. Activate by surey, you can update default settings.

## Support

If you need support on configuration and installation of this plugin : [create a support ticket on support.sondages.pro](https://support.sondages.pro/).

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab]( https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2023 Denis Chenu <https://sondages.pro>
- Copyright © 2023 OECD <https://oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
