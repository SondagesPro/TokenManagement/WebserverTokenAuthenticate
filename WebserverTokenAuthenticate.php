<?php

/**
 * Webserver authentification for registering or replace token
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2024 Denis Chenu <http://www.sondages.pro>
 * @copyright 2024 OECD <http://www.oecd.org>
 * @license AGPL v3
 * @version 0.2.3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class WebserverTokenAuthenticate extends PluginBase
{

    protected $storage = 'DbStorage';

    protected static $description = 'WebServer authentication for participant.';
    protected static $name = 'WebserverTokenAuthenticate';


    protected $settings = array(
        'serverkey' => array(
            'type' => 'string',
            'label' => 'Key for login id',
            'default' => 'REMOTE_USER',
            'help' => "Key to use for username e.g. PHP_AUTH_USER, LOGON_USER, REMOTE_USER. See phpinfo in global settings."
        ),
        'loginattribute' => array(
            'type' => 'select',
            'label' => 'Attribute to save login id',
            'options' => array(
                'firstname' => 'firstname',
                'lastname' => 'lastname',
                'email' => 'email',
                'participant_id' => 'participant_id',
                'attribute' => 'Attribute by description',
            ),
            'default' => 'lastname'
        ),
        'lowercase' => array(
            'type' => 'boolean',
            'label' => 'Use lower comparaison when find attribute.',
            'help' => 'This save the current value as lower when create too.',
            'default' => 1,
        ),
        'stripdomain' => array(
            'type' => 'boolean',
            'label' => 'Strip domain part (DOMAIN\\USER or USER@DOMAIN)',
            'default' => 0,
        ),
        'descriptionloginattribute' => array(
            'type' => 'string',
            'label' => 'Description for extra attribute',
            'default' => '',
            'help' => "Set description on any attribute here if you select “attribute by description”. If the attribute don't exist, this disable usage of LDAP access even if forced."
        ),
        'adminaccess' => array(
            'type' => 'boolean',
            'label' => 'Disable Xebserver authentication for admin user with rights on survey.',
            'help' => 'Permission checked are responses edit and tokens read',
            'default' => 1,
        ),
    );

    /** @inheritdoc */
    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');
        //
        /* Update twig file */
        //$this->subscribe('getValidScreenFiles');
        /* Show form if needed */
        $this->subscribe('beforeControllerAction');
    }

    /**
     * The settings function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings(int $surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }

        $language = $oSurvey->language;
        $aSurveyLanguage = $oSurvey->getAllLanguages();
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            'Webserver Authentication settings' => $this->gT("Webserver Authentication settings"),
            'Description' => $this->gT("Description"),
            'Description text in %s (%s)' => $this->gT("Error description text in %s (%s)"),
            'Introduction' => $this->gT("Introduction"),
            'Introduction text in %s (%s)' => $this->gT("Error Introduction text in %s (%s)"),
        );
        $aData['errors'] = array();

        /* @var array[] aSettings for SettingsWidget */
        $aSettings = array();

        $aTokens = array(
            'firstname' => gT("First name"),
            'lastname' => gT("Last name"),
            'email' => gT("Email"),
            'participant_id' => gT("Participant ID"),
        );
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokens[$attribute] = empty($information['description']) ? $attribute : $information['description'];
        }
        $defaultAttribute = $this->get("loginattribute", null, null, null);
        $emptyAttributeString = $this->gt("Leave default (mail)");
        if ($defaultAttribute) {
            $emptyAttributeString = sprintf($this->gt("Leave default : %s"), $defaultAttribute);
            if ($defaultAttribute == "attribute") {
                $descriptionAttribute = $this->get("descriptionloginattribute", null, null, null);
                if (empty($descriptionAttribute)) {
                    $emptyAttributeString = $this->gT("No default set");
                } else {
                    $attribute = $this->getUidAttribute($surveyId);
                    if (empty($attribute)) {
                        $emptyAttributeString = sprintf($this->gT("Atttribute %s didn't exist in this survey."), CHtml::encode($descriptionAttribute));
                    } else {
                        $emptyAttributeString = sprintf($this->gT("Leave default (%s)."), CHtml::encode($descriptionAttribute));
                    }
                }
            }
        }

        $allowRegister = $oSurvey->getIsAllowRegister() ? gT('Yes') : gT('No');
        $lowercaseDefault = $this->get('lowercase', null, null, $this->settings['lowercase']['default']) ? gT('Yes') : gT('No');
        $stripdomainDefault = $this->get('stripdomain', null, null, $this->settings['stripdomain']['default']) ? gT('Yes') : gT('No');

        $aWebserverSettings = array(
            'active' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => gT("No"),
                ),
                'label' => $this->gT('Use Webserver authenticate'),
                'current' => $this->get('active', 'Survey', $surveyId, ''),
                'help' => $this->gT("Activate only for token enable survey."),
            ),
            'forced' => array(
                'type' => 'select',
                'options' => array(
                    'never' => $this->gT("Never"),
                    'always' => $this->gT("Always"),
                ),
                'htmlOptions' => array(
                    'empty' => $this->gT("Automatic"),
                ),
                'label' => $this->gT('Force Webserver authenticate'),
                'current' => $this->get('forced', 'Survey', $surveyId, ''),
                'help' => $this->gT("Always totally disable usage of single token, only allowed to enter in survey after Webserver authentication. If token is allowed : it can come from url and a the form show an password input. Automatic check if attribute used for Webserver is is empty or not."),
            ),
            'create' => array(
                'type' => 'select',
                'options' => array(
                    'Y' => $this->gT("Yes"),
                    'N' => $this->gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use allow register (%s)"), $allowRegister)),
                ),
                'label' => $this->gT('Allow creation of token by Webserver account.'),
                'current' => $this->get('create', 'Survey', $surveyId, ''),
            ),
            'serverkey' => array(
                'type' => 'string',
                'label' => $this->gT('Key for login id'),
                'help' => $this->gT("Key to use for username e.g. PHP_AUTH_USER, LOGON_USER, REMOTE_USER. See phpinfo in global settings."),
                'htmlOptions' => array(
                    'placeholder' => $this->get('serverkey', null, null, $this->settings['serverkey']['default']),
                ),
                'current' => $this->get('serverkey', 'Survey', $surveyId, ''),
            ),
            'loginattribute' => array(
                'type' => 'select',
                'options' => $aTokens,
                'htmlOptions' => array(
                    'empty' => $emptyAttributeString
                ),
                'label' => $this->gT('Attribute to be used to save login'),
                'current' => $this->get('loginattribute', 'Survey', $surveyId, '')
            ),
            'lowercase' => array(
                'type' => 'select',
                'options' => array(
                    '1' => $this->gT("Yes"),
                    '0' => $this->gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), $lowercaseDefault)),
                ),
                'label' => $this->gT('Search with lower comparaison.'),
                'current' => $this->get('lowercase', 'Survey', $surveyId, ''),
            ),
            'stripdomain' => array(
                'type' => 'select',
                'options' => array(
                    '1' => $this->gT("Yes"),
                    '0' => $this->gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), $stripdomainDefault)),
                ),
                'label' => $this->gT('Strip domain part (DOMAIN\\USER or USER@DOMAIN).'),
                'current' => $this->get('stripdomain', 'Survey', $surveyId, ''),
            ),
        );
        $aSettings[$this->gT("Webserver authentication")] = $aWebserverSettings;

        $contentLanguage = "";
        $aWebserverIntroduction = $this->get('WebserverIntroduction', 'Survey', $surveyId, array());
        $aWebserverDescription = $this->get('WebserverDescription', 'Survey', $surveyId, array());

        //$contentLanguage .= $this->renderPartial('admin.DefaultLanguageSetting', $aData, true);
        foreach ($aSurveyLanguage as $languageCode) {
            $sWebserverIntroduction = isset($aWebserverIntroduction[$languageCode]) ? trim($aWebserverIntroduction[$languageCode]) : "";
            $aIntroductionContentData = array(
                'name' => "WebserverIntroduction_{$languageCode}",
                'placeholder' => $this->gT("You need to be loggued to access this survey.", 'unescaped', $languageCode),
                'value' => $sWebserverIntroduction,
                'languagecode' => $languageCode,
                'languageDetail' => getLanguageDetails($languageCode),
            );
            $contentLanguage .= $this->renderPartial('admin.IntroductionSetting', array_merge($aData, $aIntroductionContentData), true);

            $sWebserverDescription = isset($aWebserverDescription[$languageCode]) ? trim($aWebserverDescription[$languageCode]) : "";
            $aDescriptionContentData = array(
                'name' => "WebserverDescription_{$languageCode}",
                'placeholder' => "",
                'value' => $sWebserverDescription,
                'languagecode' => $languageCode,
                'languageDetail' => getLanguageDetails($languageCode),
            );
            $contentLanguage .= $this->renderPartial('admin.DescriptionSetting', array_merge($aData, $aDescriptionContentData), true);
        }
        $aSettings[$this->gT("Authentication Webserver text for error")] = array(
            'WebserverLanguage' => array(
                'type' => 'info',
                'content' => $contentLanguage,
            ),
        );

        /* Plugin event */
        $aData['aSurveyPluginsSettings'] = array();
        $beforeReloadAnyResponseSurveySettings = new PluginEvent('beforeWebserverTokenAuthenticateSurveySettings');
        $beforeReloadAnyResponseSurveySettings->set('survey', $surveyId);
        $beforeReloadAnyResponseSurveySettings->set('surveyId', $surveyId);
        App()->getPluginManager()->dispatchEvent($beforeReloadAnyResponseSurveySettings);
        $aSurveyPluginsSettings = $beforeReloadAnyResponseSurveySettings->get('settings');
        if (!empty($aSurveyPluginsSettings)) {
            $aData['aSurveyPluginsSettings'] = $aSurveyPluginsSettings;
        }
        $aData['aSettings'] = $aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this), 'method' => 'actionSaveSettings', 'surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this), 'method' => 'actionSettings', 'surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }

    /**
     * The save setting function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings(int $surveyId)
    {
        if (empty(App()->getRequest()->getPost('save' . get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $inputs = array(
            'active',
            'forced',
            'create',
            'serverkey',
            'loginattribute',
            'lowercase',
            'stripdomain'
        );
        foreach ($inputs as $input) {
            $this->set(
                $input,
                App()->getRequest()->getPost($input),
                'Survey',
                $surveyId
            );
        }

        $pluginSettings = App()->request->getPost('plugin', array());
        foreach ($pluginSettings as $plugin => $settings) {
            $settingsEvent = new PluginEvent('newWebserverTokenAuthenticateSurveySettings');
            $settingsEvent->set('settings', $settings);
            $settingsEvent->set('survey', $surveyId);
            $settingsEvent->set('surveyId', $surveyId);
            App()->getPluginManager()->dispatchEvent($settingsEvent, $plugin);
        }
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl(
                'surveyAdministration/view',
                array('surveyid' => $surveyId)
            );
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $aLangInputs = array(
            'WebserverIntroduction',
            'WebserverDescription',
        );
        foreach ($aLangInputs as $input) {
            $postedValues = array();
            foreach ($oSurvey->getAllLanguages() as $lang) {
                $postedValues[$lang] = App()->getRequest()->getPost($input . "_" . $lang);
            }
            $this->set(
                $input,
                $postedValues,
                'Survey',
                $surveyId
            );
        }
        $redirectUrl = Yii::app()->createUrl(
            'admin/pluginhelper/sa/sidebody',
            array('plugin' => get_class($this), 'method' => 'actionSettings', 'surveyId' => $surveyId)
        );
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * see beforeSurveyPage
     */
    public function beforeControllerAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get("controller") != "survey" || $this->getEvent()->get("action") != "index") {
            return;
        }
        $surveyId = App()->getRequest()->getQuery('sid');
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            return;
        }
        if (App()->getRequest()->getParam('action') == 'previewgroup' || App()->getRequest()->getParam('action') == 'previewquestion') {
            return;
        }
        if (!$oSurvey->getHasTokensTable()) {
            return;
        }
        $active = $this->get('active', 'Survey', $surveyId, false);
        if (!$active) {
            return;
        }
        /* Must check if $attribute is here */
        $attribute = $this->getUidAttribute($surveyId);

        if (empty($attribute)) {
            return;
        }
        $forced = $this->get('forced', 'Survey', $surveyId, '');
        $token = \App()->getRequest()->getParam('token');
        $tokenSession = null;
        if (App()->getRequest()->getParam('newtest') != 'Y' && isset($_SESSION['survey_' . $surveyId]['token'])) {
            $tokenSession = $_SESSION['survey_' . $surveyId]['token'];
        }
        if (empty($token)) {
            $token = $tokenSession;
        }

        /* If session is already started but different than param : must delete */
        if ($tokenSession && $token != $tokenSession) {
            unset($tokenSession);
            unset($_SESSION['survey_' . $surveyId]['token']);
        }
        /* Return if currently allowed */
        $adminaccess = $this->get('adminaccess', 'Survey', $surveyId, $this->get('adminaccess', null, null, 1));
        if ($token) {
            if ($forced == "never") {
                /* No need to check */
                return;
            }
            if ($adminaccess) {
                $permission = Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update') && Permission::model()->hasSurveyPermission($surveyId, 'tokens', 'read');
                if ($permission) {
                    /* Admin access */
                    return;
                }
            }
        } elseif (defined('\reloadAnyResponse\Utilities::API')) {
            if (App()->getRequest()->getParam('srid') && \reloadAnyResponse\Utilities::getSetting($surveyId, 'allowAdminUser')) {
                if (\Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update')) {
                    return;
                }
            }
        }
        if ($forced == "always" || !$token) {
            /* Start showing */
            $this->checkWebserverUser($surveyId);
            return;
        }

        /* we know $forced == "auto" and we have a token */
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken)) {
            return;
        }
        if (empty($oToken->$attribute)) {
            return;
        }
        /* Start showing */
        $this->checkWebserverUser($surveyId);
    }

    /**
     * The action : auto login or create a token if needed, throw 401 or leav core error if unable
     * @param integer $surveyId
     * @throws Exception 401
     * @return void
     **/
    private function checkWebserverUser($surveyId)
    {
        $webserverkey = $this->getSurveySetting($surveyId, 'serverkey');
        $attribute = $this->getUidAttribute($surveyId);
        $forced = $this->getSurveySetting($surveyId, 'forced');
        $create = $this->getSurveySetting($surveyId, 'create');
        $lowercase = $this->getSurveySetting($surveyId, 'lowercase');
        $stripdomain = $this->getSurveySetting($surveyId, 'stripdomain');
        $token = App()->getRequest()->getParam("token");

        if (empty($_SERVER[$webserverkey])) {
            if ($forced == 'always') {
                $this->ThrowErrorAccess($surveyId);
            }
            $attributevalue = null;
        } else {
            $attributevalue = $_SERVER[$webserverkey];
            if ($lowercase) {
                $attributevalue = strtolower($attributevalue);
            }
        }
        if ($stripdomain) {
            if (strpos($attributevalue, "\\") !== false) {
                // Get username for DOMAIN\USER
                $attributevalue = substr($attributevalue, strrpos($attributevalue, "\\") + 1);
            } elseif (strpos($attributevalue, "@") !== false) {
                // Get username for USER@DOMAIN
                $attributevalue = substr($attributevalue, 0, strrpos($attributevalue, "@"));
            }
        }
        if (!empty($token)) {
            $oToken = Token::model($surveyId)->findByToken($token);
            if (empty($oToken)) { // Let LS core return the error
                return;
            }
            if (empty($oToken->getAttribute($attribute)) && $forced == 'auto') {
                return;
            }
            if ($attributevalue == $oToken->getAttribute($attribute)) {
                // Already identified
                return;
            }
            if ($lowercase && $attributevalue == strtolower($oToken->getAttribute($attribute))) { // Just in case 
                // Already identified
                return;
            }
            // Token mismatch (check if needed, break reloadAnyReponse in case of reloading via token + srid)
            $this->throwMismatchAccess($surveyId);
        }
        /* No token in param, but seems identified */
        if (empty($attributevalue)) {
            // Can not work 
            return;
        }
        /* Find related token */
        $oToken = Token::model($surveyId)->findByAttributes([$attribute => $attributevalue]);
        if (!$oToken) {
            if ($create == 'N') {
                // LimeSurvey do the job
                return;
            }
            if (empty($create) && !Survey::model()->findByPk($surveyId)->getIsAllowRegister()) {
                // LimeSurvey do the job
                return;         
            }
            $oToken = Token::create($surveyId);
            $oToken->generateToken();
            $oToken->setAttribute($attribute, $attributevalue);
            if (!$oToken->encryptSave(false)) {
                throw new CHttpException(500, CHtml::errorSummary($oToken));
            }
        }
        /* Set it in GET param */
        $_GET['token'] = $oToken->token;
        /* Add an event */
        $event = new PluginEvent('WebserverTokenAuthenticate');
        $event->set('surveyId', $surveyId);
        $event->set('attribute', $attribute);
        $event->set('userid', $attributevalue);
        $event->set('oToken', $oToken);
        App()->getPluginManager()->dispatchEvent($event);
        return;
    }
    /**
     * see beforeToolsMenuRender event
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->gT('Webserver authentication'),
            'iconClass' => 'fa fa-user-circle',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $this->getEvent()->append('menuItems', array($menuItem));
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    public function gT($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($string, $sEscapeMode, $sLanguage);
    }

    /**
     * Get the current attribute set
     * @param $surveyid
     * @return string|null
     */
    private function getUidAttribute($surveyId)
    {
        $attribute = $this->get('loginattribute', 'Survey', $surveyId, '');
        if ($attribute === "") {
            $attribute = $this->get("loginattribute", null, null, null);
        }
        $aBaseAttributes = array(
            'firstname',
            'lastname',
            'email',
            'participant_id',
        );
        if (in_array($attribute, $aBaseAttributes)) {
            return $attribute;
        }
        $surveyAttributes = Survey::model()->findByPk($surveyId)->getTokenAttributes();
        if (array_key_exists($attribute, $surveyAttributes)) {
            return $attribute;
        }
        $descriptionloginattribute = $this->get('descriptionloginattribute', null, null, '');
        if (empty($descriptionloginattribute)) {
            return null;
        }

        foreach ($surveyAttributes as $attribute => $information) {
            if (!empty($information['description']) && $information['description'] == $descriptionloginattribute) {
                return $attribute;
            }
            if (empty($information['description']) && $attribute == $descriptionloginattribute) {
                return $attribute;
            }
        }
    }

    /**
     * Get current settings for a survey
     * Return surey settings if is set or not empty string, retune global if not
     * @param integer $surveyId
     * @param string $setting
     * @return mixed
     */
    private function getSurveySetting($surveyId, $setting)
    {
        $value = $this->get($setting, 'Survey', $surveyId, "");
        if ($value === "") {
            $value = $this->get($setting, null, null, '');
            if ($value === "" and isset($this->settings[$setting]['default'])) {
                $value = $this->settings[$setting]['default'];
            }
        }
        return $value;
    }

    /**
     * Throw 401 error if not loggued
     * @param integer $surveyId
     * @throw exception
     * @return void
     */
    private function ThrowErrorAccess($surveyId)
    {
        /* Validate language */
        $currentLang = App()->getRequest()->getParam('lang', App()->getLanguage());
        if (!in_array($currentLang, Survey::model()->findByPk($surveyId)->getAllLanguages())) {
            $currentLang = Survey::model()->findByPk($surveyId)->language;
        }
        $aSurveyInfo = getSurveyInfo($surveyId, $currentLang);
        $title = $this->gT("You need to be loggued to access this survey.");
        $message = "";
        $aWebserverIntroduction = $this->get('WebserverIntroduction', 'Survey', $surveyId, array());
        if (!empty($aWebserverIntroduction[$currentLang])) {
            $title = $aWebserverIntroduction[$currentLang];
        }
        $aWebserverDescription = $this->get('WebserverDescription', 'Survey', $surveyId, array());
        if (!empty($aWebserverDescription[$currentLang])) {
            $message = $aWebserverDescription[$currentLang];
        }
        $aSurveyInfo['aError'] = array(
            'plugin' => 'WebserverTokenAuthenticate',
            'error' => "401 Unauthorized",
            'title' => $title,
            'message' => $message,
        );
        Template::model()->getInstance('', $surveyId);
        header($_SERVER["SERVER_PROTOCOL"] . " " . "401 Unauthorized", true, 401);
        Yii::app()->twigRenderer->renderTemplateFromFile(
            "layout_errors.twig",
            array('aSurveyInfo' => $aSurveyInfo),
            false
        );
        App()->end();
    }

    /**
     * Throw 403 error in case of token mismatch
     * @param integer $surveyId
     * @throw exception
     * @return void
     */
    private function ThrowMismatchAccess($surveyId)
    {
        /* Validate language */
        $currentLang = App()->getRequest()->getParam('lang', App()->getLanguage());
        if (!in_array($currentLang, Survey::model()->findByPk($surveyId)->getAllLanguages())) {
            $currentLang = Survey::model()->findByPk($surveyId)->language;
        }
        $aSurveyInfo = getSurveyInfo($surveyId, $currentLang);
        $title = $this->gT('Access code mismatch');
        $message = $this->gT('Your current web authentication didn\'t allow you to get to use this access code');
        $aSurveyInfo['aError'] = array(
            'plugin' => 'WebserverTokenAuthenticate',
            'error' => "401 Unauthorized",
            'title' => $title,
            'message' => $message,
        );
        Template::model()->getInstance('', $surveyId);
        header($_SERVER["SERVER_PROTOCOL"] . " " . "401 Unauthorized", true, 401);
        Yii::app()->twigRenderer->renderTemplateFromFile(
            "layout_errors.twig",
            array(
                'aSurveyInfo' => $aSurveyInfo
            ),
            false
        );
        App()->end();
    }
}
